import { constantName } from "../actions/action_const";
const initialState = {};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case constantName:
      return { ...state, ...payload };

    default:
      return state;
  }
};
