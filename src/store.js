import { createStore, applyMiddleware, compose } from "redux";
import { logger } from "redux-logger";
import thunk from "redux-thunk";
import reducer from "./reducers";

const mware = [thunk];
let composeEnhancers = compose;
if (process.env.NODE_ENV !== "production") {
  mware.push(logger);
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}
const store = createStore(
  reducer,
  {},
  composeEnhancers(applyMiddleware(...mware))
);

export default store;
